
// var clip = new Clipboard('#copy-button');


var pdfDoc = null,
  page = 0,
   incdata = 0,
    eventdata = 0,
  comicPages = 1,
  pageNum = 1,
  pageRendering = false,
  pageNumPending = null,
  comicname = '',
  newuploadArray =[],
  comicsArray = [],
  comicDetails = [],
  comicDataPositions = [],
  scale = 1,
  canvas = document.getElementById('the-canvas'),
  ctx = canvas.getContext('2d');

/**
 * Get page info from document, resize canvas accordingly, and render page.
 * @param num Page number.
 */

  comicTrigger(page);

$('#edit-btn').on('click', function(){
  $('#form-edit').fadeToggle(300);
  $(this).hide(300);
  page++;
});

$('#next-cutout').on('click', function(){

  $('#comic-left').val('');
  $('#comic-right').val('');
  $('#comic-top').val('');
  $('#comic-zoom').val('');
  $('#comic-height').val('');
  $('#comic-width').val('');
  $('#cutout-width').val('');
  $('#cutout-height').val('');
  $('#cutout-top').val('');
  $('#cutout-left').val('');
  page++;
  console.log(page);

  $('#comic-left').attr('placeholder', comicDataPositions[page - 1].comic.left.replace('px', ''));
  $('#comic-right').attr('placeholder', comicDataPositions[page - 1].comic.right.replace('px', ''));
  $('#comic-top').attr('placeholder', comicDataPositions[page - 1].comic.top.replace('px', ''));
  $('#comic-zoom').attr('placeholder', comicDataPositions[page - 1].comic.zoom.replace('px', ''));
  $('#comic-height').attr('placeholder', comicDataPositions[page - 1].comic.top.replace('px', ''));
  $('#comic-width').attr('placeholder', comicDataPositions[page - 1].comic.top.replace('px', ''));
  $('#cutout-width').attr('placeholder', comicDataPositions[page - 1].cutout.width.replace('px', ''));
  $('#cutout-height').attr('placeholder', comicDataPositions[page - 1].cutout.height.replace('px', ''));
  $('#cutout-top').attr('placeholder', comicDataPositions[page - 1].cutout.top.replace('px', ''));
  $('#cutout-left').attr('placeholder', comicDataPositions[page - 1].cutout.left.replace('px', ''));
  comicTrigger(page);
});


$('#save-edit').on('click', function(){
    var data  = '';
    if(comicDetails.length === 0){
      data = JSON.stringify(comicDataPositions);
    }else{
      comicDetails.push(comicDataPositions);
      data = JSON.stringify(comicDetails);
    }

    console.log(data);

});


// $('#next-page').on('click', function(){
//   eventdata += 1;
//   comicDetails.push(comicDataPositions);
//   var lastitem = comicDataPositions.length - 1;
//   $('#form-edit').fadeOut(300);
//   $('#comic-left').val('');
//   $('#comic-top').val('');
//   $('#comic-zoom').val('');
//   $('#comic-width').val('');
//   $('#cutout-width').val('');
//   $('#cutout-height').val('');
//   $('#cutout-top').val('');
//   $('#cutout-left').val('');
//   $('#form-edit').fadeIn(300);
//   page = 0;
//   comicPages++;

//   // $('#comic-image').attr('src', window.location.href + 'comics/comic'+comicPages+'.png');
  
//   // $('#comic-image').attr('src', '');
//   page++;
//   comicTrigger(page);
// });


function comicTrigger(coords) {
  comicDataPositions.push({
    comicPage: comicPages,
      id:page,
      comicurl: comicname,
      comic: {
        left: '',
        right: '',
        top: '', 
        zoom: '1',
        width: '',
        height:''
      },
      cutout: {
        width:'', 
        height: '',
        top: '', 
        left:''
      }
  });

  $('.resize-container').css({ display: 'block'});
  $('.resize-container').animate({ opacity: 1 }, 100);
  comicEvents(comicDataPositions);
}

$('.toggle-cutout').on('click', function(){
  $('.resize-drag').show();
});

function comicEvents(comicDataPositions){
  $('#comic-left').keyup(function() {
    $('#comic-right').val('');
    var data = this.value + 'px';
      comicDataPositions[page].comic.right = '';
      comicDataPositions[page].comic.left = data;
    if(data === '0px'){
      $('#comic-image').css({'right' :''});
    }else{
      $('#comic-image').css({'left' : ''});
      $('#comic-image').css({'right' : data});
    }
  });

 $('#comic-right').keyup(function() {
   $('#comic-left').val('');
    var data = this.value + 'px';
      comicDataPositions[page].comic.left = '';
      comicDataPositions[page].comic.right = data;
    if(data === '0px'){
      $('#comic-image').css({'left' :''});
    }else{
      $('#comic-image').css({'right' : ''});
      $('#comic-image').css({'left' : data});
    }
});

 $('#comic-top').keyup(function() {
    var data = this.value + 'px';
    if(data === '0px'){
      comicDataPositions[page].comic.top = '';
      $('#comic-image').css({'bottom' :''});
    }else{
      comicDataPositions[page].comic.top = data;
      $('#comic-image').css({'bottom' : data});
    }
  });


  $('#comic-width').keyup(function() {
    var data = this.value + 'px';

    comicDataPositions[page].comic.width = data;
    $('#comic-image').css({'width':  data });

  });


  $('#comic-height').keyup(function() {
    var data = this.value + 'px';
    comicDataPositions[page].comic.height = data;
    $('#comic-image').css({'height':  data });
  });


  $('#comic-zoom').keyup(function() {
    var data = this.value;
    comicDataPositions[page].comic.zoom = data;

    $('#comic-image').addClass('animate');
    $('.animate').css('transform', 'scale(' + data  + ')');
  });

}

  function scrollRight(property) {
    return this[0].scrollWidth - (this[0].scrollLeft + this[0].clientWidth) + 1;
  }

  interact('.resize-drag')
  .draggable({
    // enable inertial throwing
    inertia: true,
    // keep the element within the area of it's parent
    restrict: {
      restriction: 'parent',
      endOnly: true,
      elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
    },
    // enable autoScroll
    autoScroll: true,

    // call this function on every dragmove event
    onmove: dragMoveListener,
    // call this function on every dragend event
    onend: function (event) {
      var textEl = event.target.querySelector('p');

      textEl && (textEl.textContent =
        'moved a distance of '
        + (Math.sqrt(Math.pow(event.pageX - event.x0, 2) +
                     Math.pow(event.pageY - event.y0, 2) | 0))
            .toFixed(2) + 'px');
    }
  }).resizable({
    // resize from all edges and corners
    edges: { left: true, right: true, bottom: true, top: true },

    // keep the edges inside the parent
    restrictEdges: {
      outer: 'parent',
      endOnly: true,
    },

    // minimum size
    restrictSize: {
      min: { width: 100, height: 50 },
    },

    inertia: true,
  }).on('resizemove', function (event) {
    var target = event.target,
        x = (parseFloat(target.getAttribute('data-x')) || 0),
        y = (parseFloat(target.getAttribute('data-y')) || 0);


    // update the element's style
    target.style.width  = event.rect.width + 'px';
    target.style.height = event.rect.height + 'px';

    $('#cutout-width').val( Math.round(event.rect.width));
    $('#cutout-height').val( Math.round(event.rect.height));    

    comicDataPositions[page].cutout.width =   Math.round(event.rect.width) + 'px';
    comicDataPositions[page].cutout.height =  Math.round(event.rect.height) + 'px';

    // translate when resizing from top or left edges
    x += event.deltaRect.left;
    y += event.deltaRect.top;

    target.style.webkitTransform = target.style.transform =
        'translate(' + x + 'px,' + y + 'px)';

        

    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
    target.textContent = Math.round(event.rect.width) + '\u00D7' + Math.round(event.rect.height);
  });


  function dragMoveListener (event) {
    var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

        $('#cutout-top').val(Math.round(y));
        $('#cutout-left').val(Math.round(x));

    comicDataPositions[page].cutout.left =   Math.round(x) + 'px';
    comicDataPositions[page].cutout.top =  Math.round(y) + 'px';

    // translate the element
    target.style.webkitTransform =
    target.style.transform =
      'translate(' + x + 'px, ' + y + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);
  }

    // this is used later in the resizing and gesture demos
    window.dragMoveListener = dragMoveListener;


    function readURL(input, data) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();

          reader.onload = function(e) {
            comicsArray.push(e.target.result);
            if(data === 1){
              $('#comic-image').attr('src', e.target.result);
            }
            $('#page-name').val(input.files[0].name);
            comicname = input.files[0].name;
            comicDataPositions[page - 1].comicurl = input.files[0].name;
          }
          reader.readAsDataURL(input.files[0]);
      }
    }
    
    $('#fileToUpload').change(function() {
      readURL(this);
    });

$('#pagesetup-btn').on('click', function(){
  $('#pagesetup-edit  ').show();
});


    $('.addpages').on('click', function(){
      incdata += 1;
      $('.ul-pagesinput').append('<li><input type="file" name="fileToUpload[]" id="fileToUpload" class="fileimage' + incdata  + '" onchange="readURL(this, '+incdata+')"></li>');
    
    });

// var uploadArray = [];
//     $('#pagesetup-edit').on('submit',(function(e) {
//       e.preventDefault();
//       $.ajax({
//         url: 'upload.php',
//         type: 'POST',
//         data:  new FormData(this),
//         contentType: false,
//         cache: false,
//         processData:false,
//         success: function(data){
//           uploadArray.push(data);
//           newUploadArray = JSON.parse(uploadArray);

//         // $('#gallery').html(data);
//         },
//         error: function(){} 	        
//       });
//       }));

//        var clicks = 0;
//       $('#next-page').on('click', function(){
//         clicks += 1;
//         var currentPage = jQuery.parseJSON(comicData);

//           if(clicks === currentPage.length){
//             clicks = 0;
//           }else{
//             $('#comic-image').attr('src', URL+'uploads/'+currentPage[clicks]);
//           }
//       })
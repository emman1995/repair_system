<?php

require 'libs/Bootstrap.php';
require 'libs/Database.php';
require 'libs/Controller.php';
require 'libs/Model.php';
require 'libs/View.php';
require 'libs/Session.php';

require 'config/database.php';
require 'config/paths.php';
require 'config/details.php';

$app = new Bootstrap();

?>
-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2019 at 03:58 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `marketplace_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(255) NOT NULL,
  `user_id` int(5) NOT NULL,
  `posts` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `posts`) VALUES
(84, 19, '{\"username\":\"Emman Agan\",\"user_posts_date\":\"1555646621\",\"user_posts_title\":\"This is a post\",\"user_posts_category\":\"option 2\",\"user_posts_description\":\"test\",\"postsImagesHash\":\"EmmanAgan_1555646621_54155024_2136542559763627_3246762892275482624_o.jpg\"}'),
(85, 38, '{\"username\":\"Test Test\",\"user_posts_date\":\"1555722829\",\"user_posts_title\":\"This is my postss\",\"user_posts_category\":\"Category\",\"user_posts_description\":\"asdadasdasd\",\"postsImagesHash\":\"TestTest_1555722829_53800869_2143425872408629_5618508390780108800_o.jpg\"}'),
(86, 38, '{\"username\":\"Test Test\",\"user_posts_date\":\"1555722890\",\"user_posts_title\":\"dfgbdfgdfghdfghdfgh\",\"user_posts_category\":\"Category\",\"user_posts_description\":\"asdasdasdasd\",\"postsImagesHash\":\"TestTest_1555722890_test.jpg\"}'),
(87, 38, '{\"username\":\"Test Test\",\"user_posts_date\":\"1555722903\",\"user_posts_title\":\"cvbxcvbxcvb\",\"user_posts_category\":\"option 5\",\"user_posts_description\":\"xcvbxcbxcvb\",\"postsImagesHash\":\"\"}'),
(88, 38, '{\"username\":\"Test Test\",\"user_posts_date\":\"1555722934\",\"user_posts_title\":\"asdfasdfasdf\",\"user_posts_category\":\"Category\",\"user_posts_description\":\"asdfasfasdf\",\"postsImagesHash\":\"TestTest_1555722934_depositphotos_152568680-stock-illustration-certificate-of-appreciation-template-with.jpg\"}'),
(89, 38, '{\"username\":\"Test Test\",\"user_posts_date\":\"1555722941\",\"user_posts_title\":\"asdfasdfasdf\",\"user_posts_category\":\"Category\",\"user_posts_description\":\"asdfasdfasdf\",\"postsImagesHash\":\"TestTest_1555722941_currenttopics-2.jpg\"}'),
(90, 38, '{\"username\":\"Test Test\",\"user_posts_date\":\"1555722949\",\"user_posts_title\":\"asdfasdfasdfcv\",\"user_posts_category\":\"option 3\",\"user_posts_description\":\"cxvbxcvbxcvbxcvb\",\"postsImagesHash\":\"TestTest_1555722949_currenttopics-1.jpg\"}'),
(91, 19, '{\"username\":\"Emman Agan\",\"user_posts_date\":\"1555723030\",\"user_posts_title\":\"wewe\",\"user_posts_category\":\"Category\",\"user_posts_description\":\"asdasdasd\",\"postsImagesHash\":\"EmmanAgan_1555723030_currenttopics-1.jpg\"}'),
(92, 19, '{\"username\":\"Emman Agan\",\"user_posts_date\":\"1555723041\",\"user_posts_title\":\"xcvbxcvbxcvb\",\"user_posts_category\":\"Category\",\"user_posts_description\":\"asdfasdfasdf\",\"postsImagesHash\":\"EmmanAgan_1555723041_depositphotos_152568680-stock-illustration-certificate-of-appreciation-template-with.jpg\"}'),
(93, 19, '{\"username\":\"Emman Agan\",\"user_posts_date\":\"1555723049\",\"user_posts_title\":\"xcvbncvbncvbn\",\"user_posts_category\":\"Category\",\"user_posts_description\":\"asdfasdfasdf\",\"postsImagesHash\":\"EmmanAgan_1555723049_57238868_378518356075676_3300874871705174016_n.jpg\"}');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` int(1) NOT NULL,
  `posts_count` int(100) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `username`, `password`, `role`, `posts_count`) VALUES
(19, 'Emman, Agan', 'emman@test.com', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 0),
(38, 'test, test', 'test@test.com', 'test', '098f6bcd4621d373cade4e832627b4f6', 1, 0),
(39, 'testuser, testuser', 'testuser', 'testuser', '5d9c68c6c50ed3d02a2fcf54f63993b6', 1, 0),
(40, 'wewewe, wewewe', 'wewewe', 'wewewe@wewewe', '166f8c047706e4564c329df329492c8a', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
